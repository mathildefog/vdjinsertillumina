#!/bin/bash
FOLDER=$1
SCRIPTSFOLDER=$2

#we launch a jar to create the fasta file of the uniprot entries
java -jar $SCRIPTSFOLDER/CreateFastaWithUniProtEntries.jar

#launch blast all uniprot entries VS contig sequences
blastp -query $FOLDER/analyse/contigProtSequences_forUniProt.fasta -subject $FOLDER/analyse/uniprotEntries.fasta -outfmt "7 qseqid qlen qstart qend sseqid slen length pident evalue bitscore sstart send" -out $FOLDER/analyse/contig_uniprot_blast.out 

#Analyse blast output + uniprot output and create a new tsv file
java -jar $SCRIPTSFOLDER/AnalyseUniprotOutput.jar
