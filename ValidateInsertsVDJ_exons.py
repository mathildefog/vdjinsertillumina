import pysam
import argparse
import sys
import re

parser = argparse.ArgumentParser(
    description='Filter a paired-end BAM file to find over cover regions such as mini cover >= a threshold.')
parser.add_argument('input_bamFile', help="input bam file name")
parser.add_argument('donor', help="donor")
parser.add_argument('coverage', type=int, help="minimum coverage")
parser.add_argument('chain', help="chain")

args = parser.parse_args()
if (args.input_bamFile == None or args.donor == None or args.coverage == None or args.chain == None):
    parser.print_help()
    sys.exit()
print(args.chain,"")
# Now we use only the 2 extremities of the Heavy locus region
startHeavyLocus = 105586437
endHeavyLocus = 106879844

# chr2 for kappa
startKLocus = 88848000 
endKLocus =   90362000 

# chr22 for lambda
startLLocus = 22026076
endLLocus = 22922913


def isHeavyLightLocusRegion(chr, start):
    if args.chain == "heavy" and chr == "chr14" and start >= startHeavyLocus and start <= endHeavyLocus:
        return True;
    if args.chain == "light_VK" and chr == "chr2" and start >= startKLocus and start <= endKLocus:
        return True;
    if args.chain == "light_VL" and chr == "chr22" and start >= startLLocus and start <= endLLocus:
        return True;
    return False;


# We read the BAM file
samfile = pysam.AlignmentFile(args.input_bamFile, "rb")

insertsConfirmed = 0
selectedInserts = []
selectedInserts5Prime = []
selectedInserts3Prime = []
# First parse the file with the potential insert coordinates
# with open ("nfkbInsert.bed") as f:
with open("overCoveredRegion_" + args.donor + "_" + str(args.coverage) + "reads_V4.bed")as f:
    index = 0
    for line in f:
        index += 1
        I = line.strip().split()
        startI = int(I[1])
        endI = int(I[2])
        length = endI - startI
        readNumber = 0
        correctPairsNumber = 0
        # for each potential insert
        #print ("Processing insert ", index, " on ", I[0], " with START ", startI, " and END ", endI)
        # check that the "insert" is not in the HeavyLocus and it is not longer than 2000bp or 500bp Kathrin run
        if not isHeavyLightLocusRegion(I[0], startI) and length < 2000:
            for read in samfile.fetch(I[0], startI, endI):
                readNumber += 1
                isChimericWithHeavy = False
                mateIsMapped = False
                reg = "".join((I[0], "\t", I[1], "\t", I[2]))

                # we dont process more than 1000 reads per insert
                # if readNumber>1000:
                #   break

                # the read mapping quality has to be above 5 and we dont have read with tag XA:Z 2nd alignments
                if read.mapping_quality >= 5 and not read.has_tag("XA"):
                    # we check that the read is also chimeric with the heavy locus
                    if read.has_tag("SA"):
                        # saTag = read.get_tag("SA").split(",")
                        saTags = read.get_tag("SA").split(";")
                        for saT in saTags:
                            #print(saT)
                            if "," in saT:
                                saTag = saT.split(",")
                                if isHeavyLightLocusRegion(saTag[0], int(saTag[1])):
                                    isChimericWithHeavy = True
                        if not read.mate_is_unmapped and isChimericWithHeavy:
                            mateIsMapped=True
                        #if isChimericWithHeavy:
                            #print(args.donor, "->>>>>>>>> insert ", index, ": ", reg, " CHIMERIC")
                        if isChimericWithHeavy and mateIsMapped:
                            insertsConfirmed += 1
                            print(args.donor, "->>>>>>>>> insert ", index, ": ", reg, " confirmed!")
                            selectedInserts.append(reg)
                            break

print(args.donor, " Number of inserts (complete): ", insertsConfirmed)

samfile.close()

# print out the selected regions
f = open("selectedInsert_" + args.donor + "_" + str(args.coverage) + "reads.bed", 'w+')
for insert in selectedInserts:
    f.write(insert + '\n')
    # print >> f, insert

f = open("selectedInsert_" + args.donor + "_" + str(args.coverage) + "reads_partial_5prime.bed", 'w+')
for insert in selectedInserts5Prime:
    f.write(insert + '\n')
    # print >> f, insert

f = open("selectedInsert_" + args.donor + "_" + str(args.coverage) + "reads_partial_3prime.bed", 'w+')
for insert in selectedInserts3Prime:
    f.write(insert + '\n')

