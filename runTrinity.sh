#!/bin/bash
DONOR=$1 #argument passed in command line
FOLDER=$2
CHAIN=$3
SCRIPTSFOLDER=$4

togrep="SA:Z:.*chr14.*;"  
extension1="_L001_R1_001"
extension2="_L001_R2_001"
if [ "$CHAIN" == "light_VK" ];then
	togrep="SA:Z:.*chr2.*;"
fi
if [ "$CHAIN" == "light_VL" ];then
	togrep="SA:Z:.*chr22.*;"
fi

echo $togrep
#exit 1

while IFS= read -r line; do
	COLS=() 
	for val in $line ; do
        	COLS+=("$val")
	done
	echo "--> Processing ${COLS[1]}";	
	
	#we create a folder and will move all related file to this donor there
	mkdir -p $DONOR

	#1. Extract the reads id that map to the insert, with no XA tag, have SA tag, their mates must be mapped, and mapQ>=5
	samtools view -h -f1 -F12 -@8 -q5 $DONOR.bam ${COLS[0]} |grep -v 'XA:'| grep "$togrep"> $DONOR.${COLS[1]}.insertReads.sam
	cut -f1 $DONOR.${COLS[1]}.insertReads.sam > $DONOR.ids.${COLS[1]}.txt

	#2.HERE we use the python script to get the complete reads (not hard/soft-clipped) from the fastq file with this id list
	echo "Retrieving the reads in the fastQ file... number total of IDS:"
	wc -l $DONOR.ids.${COLS[1]}.txt
	READSNUMBER=$(wc -l < "$DONOR.ids.${COLS[1]}.txt")
	gunzip -c $FOLDER/trimmed/$DONOR$extension1"_val_1.fq.gz" > $FOLDER/align/$DONOR$extension1"_val_1.fq"
	cat $DONOR$extension1"_val_1.fq" | python $SCRIPTSFOLDER/extractReadsFromFastQFile.py $DONOR.ids.${COLS[1]}.txt 1 > $DONOR.${COLS[1]}.1.fastq
	rm -f $FOLDER/align/$DONOR$extension1"_val_1.fq"
	gunzip -c $FOLDER/trimmed/$DONOR$extension2"_val_2.fq.gz" > $FOLDER/align/$DONOR$extension2"_val_2.fq"
	cat  $DONOR$extension2"_val_2.fq" | python $SCRIPTSFOLDER/extractReadsFromFastQFile.py $DONOR.ids.${COLS[1]}.txt 2 > $DONOR.${COLS[1]}.2.fastq
	rm -f $FOLDER/align/$DONOR$extension2"_val_2.fq"

	#3. Trinity assembly
	echo "Running Trinity... with reads numb "$READSNUMBER
	Trinity --seqType fq --left $FOLDER/align/$DONOR.${COLS[1]}.1.fastq --right $FOLDER/align/$DONOR.${COLS[1]}.2.fastq --CPU 10 --max_memory 20G --min_contig_length 150 --no_normalize_reads --output $FOLDER/align/$DONOR"_"${COLS[1]}"_"trinity --full_cleanup 
	rm -f -r $FOLDER/align/$DONOR"_"${COLS[1]}"_"trinity
	rm -f align/$DONOR.${COLS[1]}.*
	find $FOLDER/align -type f -name "$DONOR.insert[0-9]*" -delete
done <"selectedInsert_"$DONOR"_CONTIG.txt"
mv $FOLDER/align/$DONOR* $FOLDER/align/$DONOR/
mv $FOLDER/align/*"_"$DONOR"_"* $FOLDER/align/$DONOR/
if [ -f $FOLDER/align/file.vcf.gz ]; then
	mv $FOLDER/align/file.vcf* $FOLDER/align/$DONOR/
fi

