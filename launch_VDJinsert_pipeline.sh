#!/bin/bash
#/home/perez/SCRIPTS/vdjInsert_PAPER/launch_VDJinsert_pipeline_diff_species.sh "/scratch/perez/INSERT/run0" &
####
####  DO not forget to create the file sampleToDonor.txt!!!
####
#set the folder where you put all the scripts
SCRIPTSFOLDER="/home/perez/SCRIPTS/vdjInsert_SCRIPTS"
IGBLASTNFOLDER="/home/perez/ncbi-igblast-1.12.0/"
GENOMEFILE="/PATHtoHg38FASTAFILE/" #hg38.fa

#abort the script if anything goes wrong, add 'exit 1' after a line if we want the script to stop
set -e

RUN=$1 #set by te user

CHAIN="heavy"
SPECIES="human"
VPRIMERS="$SCRIPTSFOLDER/VH/VHprimers_last15bp_v2.fasta"
#set of primers used for run0, run1 and run2
VPRIMERS1="$SCRIPTSFOLDER/VH/VHprimers_last15bp_v1.fasta"
CONST="$SCRIPTSFOLDER/VH/human_igh_ch1_prot.fasta"

#CHAIN="light_VK"
#SPECIES="human"
#VPRIMERS="$SCRIPTSFOLDER/VK/VKprimers_last15bp.fasta"
#CONST="$SCRIPTSFOLDER/VK/human_igk_ch1_prot.fasta"

#CHAIN="light_VL"
#SPECIES="human"
#VPRIMERS="$SCRIPTSFOLDER/VL/VLprimers_last15bp.fasta"
#CONST="$SCRIPTSFOLDER/VL/human_igL_ch1_prot.fasta"

#1.create the sampleIds.txt file
java -jar $SCRIPTSFOLDER/CreateSampleIdsFile.jar raw

exit 1

#Create the sampleToDonor.txt file! Use table_s1.xslx from the paper.

#2. Launch trim_galore for each sample
$SCRIPTSFOLDER/runTrimGalore.sh

#3. Launch BWA mem
$SCRIPTSFOLDER/runBWAmem_bySpecies.sh $SPECIES $GENOMEFILE

#4. Look for inserts and run Trinity !!!!!!!! HEAVY or LIGHT or tra or trb !!!!!!!!!!!!!!
$SCRIPTSFOLDER/lookForInserts.sh $RUN $CHAIN $SCRIPTSFOLDER

#5. Analyze the sequences
java -jar  $SCRIPTSFOLDER/AnalyzeTrinityOutput.jar 

#6. Make vcf file to get the insert consensus sequence
$SCRIPTSFOLDER/runSamtoolsMpileup_bySpecies.sh $RUN $SPECIES $GENOMEFILE

#7. Make the consensus insert sequence and launch blast
$SCRIPTSFOLDER/runBlastConsensusAgainstContig_bySpecies.sh $RUN $SPECIES $GENOMEFILE

#8. Parse the blast output
java -jar  $SCRIPTSFOLDER/AnalyseBlastOutput.jar


#9. Map exon to full exon genomic coordinates
gencodefile="$SCRIPTSFOLDER/hg38_ref_files/gencode.v29.cds.bed"
bedtools intersect -a $gencodefile -b analyse/insertList_withExon.bed -f 0.99 -wb -wa > analyse/insertList_withExon_mappedToCDS.tsv

#10. Launch IgBlast in igblast folder to get the positions
cd /home/perez/ncbi-igblast-1.12.0/

#default for heavy chain
IGBLASTCOMMAND1="-germline_db_V human_igh_V -germline_db_D human_igh_D -germline_db_J human_igh_J"
IGBLASTCOMMAND2="-db human_igh_ch1 -num_alignments_V 1 -num_alignments_D 1 -num_alignments_J 1"
OPFILE="-auxiliary_data optional_file/human_gl.aux"

if [ "$CHAIN" == "light_VK" ];then
	IGBLASTCOMMAND1="-germline_db_V human_igk_V -germline_db_D human_igh_D -germline_db_J human_igk_J"
	IGBLASTCOMMAND2="-db human_igk_ch1 -num_alignments_V 1 -num_alignments_J 1" 
fi

if [ "$CHAIN" == "light_VL" ];then
	IGBLASTCOMMAND1="-germline_db_V human_igl_V -germline_db_D human_igh_D -germline_db_J human_igl_J"
	IGBLASTCOMMAND2="-db human_igl_ch1_nuc.fasta -num_alignments_V 1 -num_alignments_J 1" 
fi

igblastn $IGBLASTCOMMAND1 -query $RUN/analyse/sequencesForIgBlast.fasta $OPFILE -show_translation -out $RUN/analyse/igBlast.out $IGBLASTCOMMAND2 -num_alignments 1 -outfmt "7 qseqid qlen qstart qend sseqid slen length pident evalue bitscore sstart send"

#To get the FR* in frame translation
igblastn $IGBLASTCOMMAND1 -query $RUN/analyse/sequencesForIgBlast.fasta $OPFILE -show_translation -out $RUN/analyse/igBlast_alignment.out $IGBLASTCOMMAND2 -num_alignments 1 

#11. ParseIgBlast output
cd $RUN
#if run0 or run1 use $VPRIMERS1 $CONST
#if run2 use $VPRIMERS $VPRIMERS1 $CONST
#else  use $VPRIMERS $CONST
java -jar  $SCRIPTSFOLDER/AnalyseIgBlastOutput_bySpecies.jar $SPECIES $VPRIMERS $CONST

exit 1

#12. RUN UniProt web service with 'geneIds_forUniProt.txt' file produced in 'analyse' folder (gene name to uniprotKB, add columns sequence + domains (Coiled coil/Compositional bias/Domain [FT]/Motif/Region/Zinc finger), reviewed and save as excel), store the ouptut file as 'uniprot_output.tab'

#13. Analyse UniProt output
$SCRIPTSFOLDER/runUniProtAnalysis.sh $RUN $SCRIPTSFOLDER

#14. Found exon-exon junctions
java -jar $SCRIPTSFOLDER/CreateInsertSequencesFastaFile.jar

# The junction files were previously generated using JAGuaR
junctionfile="$SCRIPTSFOLDER/hg38_ref_files/allJunctions_JAGUaR_hg38p12.fa"
blastn -query analyse/insertSequencesForExonExon.fasta -task megablast -db $junctionfile -outfmt "7 qseqid qlen qstart qend sseqid slen length pident evalue bitscore sstart send" -out analyse/contig_junction_blast.out -perc_identity 95

java -jar $SCRIPTSFOLDER/AnalyseExonExonJunctions.jar

java -jar $SCRIPTSFOLDER/AddDonorId.jar analyse/insertList_FINAL_withExonExonJunction.tsv sampleToDonor.txt


