#!/bin/bash
set -e
while IFS= read -r line; do
	trim_galore --nextera --paired -q 20 --length 100 raw/$line"_L001_R1_001.fastq.gz" raw/$line"_L001_R2_001.fastq.gz" --no_report_file -o trimmed 	
done < "samplesIds.txt"
