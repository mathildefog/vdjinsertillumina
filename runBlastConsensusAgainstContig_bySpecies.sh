#!/bin/bash
FOLDER=$1
SPECIES=$2
genomefile=$3
echo "The genome file used is " $genomefile

#we remove consensusInsert.fa if already exists
for file in $FOLDER/analyse/*.fa; do
	rm -f $file
done
while IFS= read -r line; do
	COLS=() 
	for val in $line ; do
        	COLS+=("$val")
	done	
	echo "--> Processing ${COLS[1]}";
	samtools faidx $genomefile ${COLS[2]} | bcftools consensus $FOLDER/align/${COLS[0]}/file.vcf.gz -o ${COLS[1]}.fa
	awk '/^>/ {gsub(/.fa$/,"",FILENAME);printf(">%s\n",FILENAME);next;} {print}' ${COLS[1]}.fa >$FOLDER/analyse/${COLS[1]}.fa
	rm -f ${COLS[1]}.fa
done<$FOLDER/analyse/insertListAfterTrinity.txt

#cat all .fa file in one -> all consensus insert sequences
cat $FOLDER/analyse/*.fa > $FOLDER/analyse/consensusInsert.fa

#launch blast concensus VS contig sequences
blastn -query $FOLDER/analyse/consensusInsert.fa -task megablast -dust no -subject $FOLDER/analyse/allContigs.fasta -outfmt "7 qseqid qlen qstart qend sseqid slen length pident evalue bitscore sstart send" -out $FOLDER/analyse/contig_insert_blast.out -perc_identity 95

