#!/bin/bash 
#~/SCRIPTS/vdjInsert_PAPER/getRawReadsNumber.sh run8  /scratch/perez/INSERT/run8

RUN=$1
FOLDER=$2 
echo $OUT
echo -e "sample\tPE.reads.number" >$FOLDER/reads_number/$RUN.reads.number.tsv
while IFS= read -r DONOR; do
	printf $DONOR >> $FOLDER/reads_number/$RUN.reads.number.tsv
	READS=$(zcat  $FOLDER/raw/$DONOR"_L001_R1_001.fastq.gz" | echo $((`wc -l`/4)))
	echo -e "\t$READS" >>$FOLDER/reads_number/$RUN.reads.number.tsv
done < $FOLDER/samplesIds.txt
