# Suppression PCR sequences analysis (Illumina) #

Copyright (C) 2022  Mathilde Foglierini Perez

email: mathilde.foglierini-perez@chuv.ch

### SUMMARY ###

We have made available here a series of scripts to analyze non-VDJ insertions in human antibody transcripts.

The scripts are primarily intended as reference for manuscript "Different classes of genomic inserts contribute to human antibody diversity" rather than a stand-alone application.

The input of the pipeline is 300 bp paired-end reads coming from a target amplicon of the antibody transcripts (heavy or light chain). Data can be found at SRA db: PRJNA638005 accession number.

The general overview of the pipeline is represented in 'SuppFig_vdj_insert_Illumina.pdf' file.
These scripts were run on Linux machines.

### LICENSES ###

This code is distributed open source under the terms of the GNU Free Documention License.


### INSTALL ###

Before the pipeline can be run, the following software are required:

a) Python 2.7 https://www.python.org/download/releases/2.7/

b) Pysam https://github.com/pysam-developers/pysam

c) Java JDK 12 https://www.oracle.com/technetwork/java/javase/downloads/index.html

d) FastQC and Trim Galore! http://www.bioinformatics.babraham.ac.uk/projects/index.html

e) Burrows-Wheeler Aligner v0.7.17 http://bio-bwa.sourceforge.net/

f) samtools v1.9 http://samtools.sourceforge.net/

g) BCFtools v1.9 https://samtools.github.io/bcftools/

h) Bedtools v2.27 http://bedtools.readthedocs.io/en/latest/index.html#

i) BEDOPS v2.4.30 https://bedops.readthedocs.io/en/latest/

j) Trinity v2.8.4 https://github.com/trinityrnaseq/trinityrnaseq/wiki

k) BLAST+ v2.7.1 ftp://ftp.ncbi.nlm.nih.gov/blast/executables/blast+/2.7.1/

l) IgBlast 1.12.0 ftp://ftp.ncbi.nih.gov/blast/executables/igblast/release/1.12.0/

### PIPELINE ###

First we download all scripts and needed files into a directory.
Then we create a directory for each run and create a hierachy as following:

 		$ /$PATHTOWORKINGDIRECTORY/$RUN  
 		$ ├── align  
 		$ ├── analyse  
 		$ ├── raw  
 		$ └── trimmed  


We move the fastq files in the raw directory.  
All the command below can be found in one single bash script: 'launch_VDJinsert_pipeline.sh'


1. Set the variables

 		$ SCRIPTSFOLDER="/$PATHTOSCRIPTS/"
 		$ GENOMEFILE="/PATHtoHg38FASTAFILE/" #hg38.fa
		$ RUN="run0" #from run0 to run9
		
		$ CHAIN="heavy"
		$ SPECIES="human"
		$ VPRIMERS="$SCRIPTSFOLDER/VH/VHprimers_last15bp_v2.fasta"
		$ #set of primers used for run0, run1 and run2
		$ VPRIMERS1="$SCRIPTSFOLDER/VH/VHprimers_last15bp_v1.fasta"
		$ CONST="$SCRIPTSFOLDER/VH/human_igh_ch1_prot.fasta"  
  
	If we analyze the Kappa transcripts

		$ CHAIN="light_VK"
		$ SPECIES="human"
		$ VPRIMERS="$SCRIPTSFOLDER/VK/VKprimers_last15bp.fasta"
		$ CONST="$SCRIPTSFOLDER/VK/human_igk_ch1_prot.fasta"  
  
	If we analyze the Lambda transcripts		

		$ CHAIN="light_VL"
		$ SPECIES="human"
		$ VPRIMERS="$SCRIPTSFOLDER/VL/VLprimers_last15bp.fasta"
		$ CONST="$SCRIPTSFOLDER/VL/human_igL_ch1_prot.fasta"  
 		
2. Create the sampleIds.txt file

		$ java -jar $SCRIPTSFOLDER/CreateSampleIdsFile.jar raw	
		
	Create the sampleToDonor.txt file: for each raw, one sampleId related to one donorId separated by a tab.
	Please use table_s1.xlsx file for the links between sampleIds and donorIds.
   
  
3. Launch trim_galore for each sample

		$ $SCRIPTSFOLDER/runTrimGalore.sh  
  
4. Launch BWA mem

		$ $SCRIPTSFOLDER/runBWAmem_bySpecies.sh $SPECIES $GENOMEFILE  
  
5. Look for inserts and run Trinity

		$ $SCRIPTSFOLDER/lookForInserts.sh $RUN $CHAIN $SCRIPTSFOLDER  
  
6. Analyze the sequences

		$ java -jar  $SCRIPTSFOLDER/AnalyzeTrinityOutput.jar 

7. Make vcf file to get the insert consensus sequence

		$ $SCRIPTSFOLDER/runSamtoolsMpileup_bySpecies.sh $RUN $SPECIES $GENOMEFILE

8. Make the consensus insert sequence and launch blast

		$ $SCRIPTSFOLDER/runBlastConsensusAgainstContig_bySpecies.sh $RUN $SPECIES $GENOMEFILE 

9. Parse the blast output

		$ java -jar  $SCRIPTSFOLDER/AnalyseBlastOutput.jar


10. Map exon to full exon genomic coordinates

		$ gencodefile="$SCRIPTSFOLDER/hg38_ref_files/gencode.v29.cds.bed"
		$ bedtools intersect -a $gencodefile -b analyse/insertList_withExon.bed -f 0.99 -wb -wa > analyse/insertList_withExon_mappedToCDS.tsv

11. Launch IgBlast in igblast folder to get the positions of the V(D)J genes

		$ cd /$IgblastnPATH/

	Default for heavy chain

		$ IGBLASTCOMMAND1="-germline_db_V human_igh_V -germline_db_D human_igh_D -germline_db_J human_igh_J"
		$ IGBLASTCOMMAND2="-db human_igh_ch1 -num_alignments_V 1 -num_alignments_D 1 -num_alignments_J 1"
		$ OPFILE="-auxiliary_data optional_file/human_gl.aux"

		$ if [ "$CHAIN" == "light_VK" ];then
		$ 	IGBLASTCOMMAND1="-germline_db_V human_igk_V -germline_db_D human_igh_D -germline_db_J human_igk_J"
		$ 	IGBLASTCOMMAND2="-db human_igk_ch1 -num_alignments_V 1 -num_alignments_J 1" 
		$ fi

		$ if [ "$CHAIN" == "light_VL" ];then
		$ 	IGBLASTCOMMAND1="-germline_db_V human_igl_V -germline_db_D human_igh_D -germline_db_J human_igl_J"
		$ 	IGBLASTCOMMAND2="-db human_igl_ch1_nuc.fasta -num_alignments_V 1 -num_alignments_J 1" 
		$ fi

		$ igblastn $IGBLASTCOMMAND1 -query $RUN/analyse/sequencesForIgBlast.fasta $OPFILE -show_translation -out $RUN/analyse/igBlast.out $IGBLASTCOMMAND2 -num_alignments 1 -outfmt "7 qseqid qlen qstart qend sseqid slen length pident evalue bitscore sstart send"

	To get the FR* in frame translation

		$ igblastn $IGBLASTCOMMAND1 -query $RUN/analyse/sequencesForIgBlast.fasta $OPFILE -show_translation -out $RUN/analyse/igBlast_alignment.out $IGBLASTCOMMAND2 -num_alignments 1 

12. ParseIgBlast output

		$ cd $RUN
		
		$ #if run0 or run1 use $VPRIMERS1 $CONST
		$ #if run2 use $VPRIMERS $VPRIMERS1 $CONST
		$ #else  use $VPRIMERS $CONST

		$ java -jar  $SCRIPTSFOLDER/AnalyseIgBlastOutput_bySpecies.jar $SPECIES $VPRIMERS $CONST

		$ exit 1
  
13. RUN UniProt web service with 'geneIds_forUniProt.txt' file produced in 'analyse' folder (gene name to uniprotKB, add columns sequence + domains (Coiled coil/Compositional bias/Domain [FT]/Motif/Region/Zinc finger), reviewed and save as excel), store the ouptut file as 'uniprot_output.tab'
  
14. Analyse UniProt output

		$ $SCRIPTSFOLDER/runUniProtAnalysis.sh $RUN $SCRIPTSFOLDER
  
15. Found exon-exon junctions

		$ java -jar $SCRIPTSFOLDER/CreateInsertSequencesFastaFile.jar

	The junction files were previously generated using JAGuaR

		$ junctionfile="$SCRIPTSFOLDER/hg38_ref_files/allJunctions_JAGUaR_hg38p12.fa"
		$ blastn -query analyse/insertSequencesForExonExon.fasta -task megablast -db $junctionfile -outfmt "7 qseqid qlen qstart qend sseqid slen length pident evalue bitscore sstart send" -out analyse/contig_junction_blast.out -perc_identity 95

		$ java -jar $SCRIPTSFOLDER/AnalyseExonExonJunctions.jar

		$ java -jar $SCRIPTSFOLDER/AddDonorId.jar analyse/insertList_FINAL_withExonExonJunction.tsv sampleToDonor.txt

