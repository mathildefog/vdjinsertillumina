#!/bin/bash
SPECIES=$1
genomefile=$2
echo "The genome file used is " $genomefile

while IFS= read -r line; do
	bwa mem -t 10 $genomefile trimmed/$line"_L001_R1_001_val_1.fq.gz" trimmed/$line"_L001_R2_001_val_2.fq.gz" | samtools view -bSu - > align/$line"_notSorted.bam"
	samtools sort align/$line"_notSorted.bam" -o align/$line.bam -@ 12
	samtools index align/$line.bam
	rm -f align/$line"_notSorted.bam"
	bedtools genomecov -dz -ibam align/$line".bam" > align/$line"_depth_v4_bedtools.txt"	
done < "samplesIds.txt"
