#!/bin/bash
FOLDER=$1 #argument passed in command line
CHAIN=$2
SCRIPTSFOLDER=$3

annotationfile="$SCRIPTSFOLDER/hg38_ref_files/gencode.v29.annotation.exon.gene_shortedV2.bed"


while IFS= read -r line; do
	cd align/
	java -jar $SCRIPTSFOLDER/FindOverCoverRegion.jar $line 10 20
	python $SCRIPTSFOLDER/ValidateInsertsVDJ_exons.py $line.bam $line 10 $CHAIN
	sort-bed "selectedInsert_"$line"_10reads.bed" > "selectedInsert_"$line"_merged_sorted.bed"
	bedmap --echo --echo-map-id-uniq --delim '\t' "selectedInsert_"$line"_merged_sorted.bed" $annotationfile > "selectedInsert_"$line"_Annotated.bed"	
	sort -k1,1V -k2,2n "selectedInsert_"$line"_Annotated.bed" > "selectedInsert_"$line"_Annotated_sorted.bed"
	java -jar $SCRIPTSFOLDER/CalculateInsertCoverage.jar $line "selectedInsert_"$line"_Annotated_sorted.bed"
	java -jar $SCRIPTSFOLDER/TsvAnnotatedToInsertId.jar $line
	$SCRIPTSFOLDER/runTrinity.sh $line $FOLDER $CHAIN $SCRIPTSFOLDER
done < "samplesIds.txt"
