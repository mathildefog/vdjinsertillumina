#!/bin/bash
FOLDER=$1
SPECIES=$2
genomefile=$3
echo "The genome file used is " $genomefile

while IFS= read -r line; do
	if [ -d $FOLDER/align/$line ]; then
		cd $FOLDER/align/$line
		#in the case of long insert we may have already ran this command
		if [ ! -f $FOLDER/align/$line/file.vcf.gz ]; then
			samtools view -h -L $line.insertListForBlast.bed $line.bam |samtools view -b -h -> $line.inserts.bam
			samtools mpileup -d8000 -uf $genomefile $line.inserts.bam | bcftools call -c - >file.vcf
			bgzip -c file.vcf > file.vcf.gz
			tabix -p vcf file.vcf.gz
		fi
	fi	
done < "samplesIds.txt"

